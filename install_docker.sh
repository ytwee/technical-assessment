#!/bin/bash
echo "Installing Docker"
sudo apt-get update
sudo apt-get install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
sudo apt-get install -y docker.io
echo "Granting docker access for user"
sudo usermod -aG docker ubuntu
echo "Running nginx docker image"
docker run -d --restart always --name nginx -p 80:80 nginx:1.18.0
echo "Editing nginx main page"
docker exec -i nginx sed -i 's|<h1>Welcome to nginx!</h1>|<h1>Hi, my name is Wee Yaw Teck! Welcome to nginx!</h1>|' /usr/share/nginx/html/index.html
