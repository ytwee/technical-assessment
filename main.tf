terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}


### AWS VPC ###
resource "aws_vpc" "demo" {
  cidr_block = "10.0.0.0/16"

  tags = {
    Name = "demo"
  }
}


### AWS Subnet ###
resource "aws_subnet" "private" {
  vpc_id            = aws_vpc.demo.id
  cidr_block        = "10.0.1.0/24"
  availability_zone = "us-east-1a"

  tags = {
    Name = "private"
  }
}

resource "aws_subnet" "public" {
  vpc_id            = aws_vpc.demo.id
  cidr_block        = "10.0.100.0/24"
  availability_zone = "us-east-1a"

  tags = {
    Name = "public"
  }
}


### AWS Internet Gateway ###
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.demo.id

  tags = {
    Name = "internet-gateway"
  }
}


### AWS Route Table ###
resource "aws_route_table" "private-rt" {
  vpc_id = aws_vpc.demo.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.demo-nat.id
  }

  tags = {
    Name = "private-route-table"
  }
}

resource "aws_route_table" "public-rt" {
  vpc_id = aws_vpc.demo.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }

  tags = {
    Name = "public-route-table"
  }
}

resource "aws_route_table_association" "private" {
  subnet_id      = aws_subnet.private.id
  route_table_id = aws_route_table.private-rt.id
}

resource "aws_route_table_association" "public" {
  subnet_id      = aws_subnet.public.id
  route_table_id = aws_route_table.public-rt.id

}


### AWS NAT Gateway ###
resource "aws_eip" "demo-nat-eip" {
  vpc = true
}

resource "aws_nat_gateway" "demo-nat" {
  allocation_id = aws_eip.demo-nat-eip.id
  subnet_id     = aws_subnet.public.id

  tags = {
    Name = "demo-nat"
  }

  # To ensure proper ordering, it is recommended to add an explicit dependency
  # on the Internet Gateway for the VPC.
  depends_on = [aws_internet_gateway.igw, aws_eip.demo-nat-eip]
}


### AWS Security Group ###
resource "aws_security_group" "demo-SG" {
  name        = "demo-SG"
  description = "Allow ssh and http access only"
  vpc_id      = aws_vpc.demo.id

  ingress {
    description = "ssh from anywhere"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "http from specific IP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["118.189.0.0/16", "116.206.0.0/16", "223.25.0.0/16", "180.75.245.132/32", aws_vpc.demo.cidr_block]
  }

  egress {
    description = "allow all traffic for outbound"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "demo-SG"
  }
}


### AWS EC2 ###
resource "aws_instance" "demo-EC2" {
  ami                         = "ami-0c4f7023847b90238"
  instance_type               = "t2.micro"
  subnet_id                   = aws_subnet.private.id
  vpc_security_group_ids      = [aws_security_group.demo-SG.id]
  key_name                    = aws_key_pair.demo-key-pair.id
  user_data                   = file("install_docker.sh")
  user_data_replace_on_change = false

  tags = {
    Name = "demo-EC2"
  }
}

resource "aws_instance" "demo-EC2-bastion-host" {
  ami                    = "ami-0c4f7023847b90238"
  instance_type          = "t2.micro"
  subnet_id              = aws_subnet.public.id
  vpc_security_group_ids = [aws_security_group.demo-SG.id]
  key_name               = aws_key_pair.demo-key-pair.id

  tags = {
    Name = "demo-EC2-bastion-host"
  }
}

resource "aws_eip" "demo-EC2-bastion-host-eip" {
  instance = aws_instance.demo-EC2-bastion-host.id
  vpc      = true
}

resource "aws_key_pair" "demo-key-pair" {
  key_name   = "demo-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCTLIYQfQKPhhljREbI6wHn5b9ZH57DALzSQ1NSD5jYgyAvQPEQuX7JP3Z8SIUzzLWQbnOO6PfHmRNO2slQkWM1guczGcLhgvJX0xEZulW+pRC4ZzJqIRXgGwSZUcm7LczrUn6HbnSbDxOl6AAfjLRHB2rFsM50tXrx2Jl5+p/QT6RFyn0CTNSl/0YBGGI0LIV6qr3iFW+yA1VLvBQ3es+7aqjFBUK6ZufQ4it+DDiFPR5B/0hlfBAnwofK13bBOA4bo1i12Ov9T5+T+76KTBBaVV+HQ3BWKPl9LX8XUv7XYfQhxu9X9JMEkxKupSxi4u8qjCd2Z6S7hzNte1APXk2j demo-ssh-access"
}


### AWS ELB ###
resource "aws_lb" "demo-lb" {
  name               = "demo-lb"
  load_balancer_type = "network"
  subnets            = [aws_subnet.public.id]

  tags = {
    Name = "demo-lb"
  }
}

resource "aws_lb_target_group" "demo-lb-tg-ssh" {
  name     = "demo-lb-tg-ssh"
  port     = 22
  protocol = "TCP"
  vpc_id   = aws_vpc.demo.id
}

resource "aws_lb_target_group" "demo-lb-tg-http" {
  name     = "demo-lb-tg-http"
  port     = 80
  protocol = "TCP"
  vpc_id   = aws_vpc.demo.id
}

resource "aws_lb_listener" "demo-lb-listener-ssh" {
  load_balancer_arn = aws_lb.demo-lb.arn
  port              = "22"
  protocol          = "TCP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.demo-lb-tg-ssh.arn
  }
}

resource "aws_lb_target_group_attachment" "demo-tg-attachment-ssh" {
  target_group_arn = aws_lb_target_group.demo-lb-tg-ssh.arn
  target_id        = aws_instance.demo-EC2.id
  port             = 22
}

resource "aws_lb_listener" "demo-lb-listener-http" {
  load_balancer_arn = aws_lb.demo-lb.arn
  port              = "80"
  protocol          = "TCP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.demo-lb-tg-http.arn
  }
}

resource "aws_lb_target_group_attachment" "demo-tg-attachment-http" {
  target_group_arn = aws_lb_target_group.demo-lb-tg-http.arn
  target_id        = aws_instance.demo-EC2.id
  port             = 80
}


### AWS IAM ###
resource "aws_iam_user" "demo-iam-user" {
  name = "demo-iam-user"
}

resource "aws_iam_user_policy_attachment" "demo-iam-user-ro" {
  user       = aws_iam_user.demo-iam-user.name
  policy_arn = "arn:aws:iam::aws:policy/ReadOnlyAccess"
}

resource "aws_iam_user_login_profile" "demo-iam-user-login" {
  user = aws_iam_user.demo-iam-user.name
}