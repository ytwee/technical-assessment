output "lb_dns_name" {
  description = "Load balancer DNS name"
  value       = aws_lb.demo-lb.dns_name
}

output "demo_ec2_bastion_host_public_ip" {
  description = "Public IP address of the demo-EC2-bastion-host"
  value       = aws_eip.demo-EC2-bastion-host-eip.public_ip
}

output "demo_ec2_private_ip" {
  description = "Private IP address of demo-EC2"
  value       = aws_instance.demo-EC2.private_ip
}

output "iam_user_id" {
  description = "IAM user name"
  value       = aws_iam_user_login_profile.demo-iam-user-login.id
}

output "iam_user_password" {
  description = "IAM user password"
  value       = aws_iam_user_login_profile.demo-iam-user-login.password
}

output "console_login_link" {
  description = "IAM user console login link"
  value       = "https://107149992457.signin.aws.amazon.com/console"
}